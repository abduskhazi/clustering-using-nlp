#Syntactic decomposition Description
#    Pre-requisite - preprocessor.py must be run before this script is run
#    It reads its input from the output of preprocessor.py i.e input_file_preprocessed.txt
#    It writes its output to 3 different places : console , Final_Output_File.txt , TableTCL.txt (Table formatted output)
#    Post the running of this script, we an expect intelligible segregation of the sentences into 5 groups namely, Person Group
#    Location Group , Date Group , Time Group , Default Group. [This sentence should be updated if we add/remove groups from our project]
#    In each group the sentences are further segragated based on particular words called labels. The criterion for selecting the
#    label depends on the group. (See below for group specific documentation)
#

#Further work
#    Currently the project takes the input from a particular input file. We can make it take the input
#    from a specified file. Exception Handling i.e if an exception is thrown anywhere in the code, we must
#    close the opened files in the exception handler/block.
#

import re
import nltk
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

#Obtaining input from the file input_file_preprocessed.txt and storing the contents of the file in the variable filedata.
#The input file is immediately closed after obtaining the contents as there is no need for the file to be open after reading its contents.
f = open("input_file_preprocessed.txt","r")
filedata = f.read()
f.close()

#The 2 output files are opened here. Please note that these files are closed only at the end as they need to be open till the last print/write
#to the file
outputFile = open("Final_Output_File.txt", "w")
TableFileName = open("TableTCL.txt", "w")


#filedata is split here based on the tokens/delimeters \n or \r or . (full stop)
#We get a list of sentences now instead of a big string content. These sentences may contain null sentences i.e ''.
#Therefore we use the condition --> if sentence != '' <-- to prevent having null sentences.
#Reading the below line like english would make the meaning clear.
listOfSentences = [ sentence for sentence in re.split('\n|\r|\.', filedata) if sentence != '' ]



#==========================================================================================================================
#========================================== UTILITY FUNCTIONS =============================================================
#==========================================================================================================================


def fillDictionary(intersectionSet, DateTableDict , monthWords, sent):
    indexMatch = -1
    if(intersectionSet):
        indexMatch = [x for x in range(len(monthWords)) if set([monthWords[x]]) & intersectionSet ][0]
    if(indexMatch != -1):
        dictionaryIndex = monthWords[2*int(indexMatch/2) + 1]
        if(dictionaryIndex not in DateTableDict):
            DateTableDict[dictionaryIndex] = list()
        DateTableDict[dictionaryIndex].append(sent)

def getDefaultGroupMatchedKey(sentence , DefaultGroupstopWords):
    key = "unKnown"
    tempSentence = str(sentence.lower()) # copy the whole sentence in tempSentences
    tempSentenceWords = tempSentence.split(' ')
    listOfPossibleKeys = [x for x in tempSentenceWords if x not in DefaultGroupstopWords and len(x) > 2]
    if( len(listOfPossibleKeys) > 0 ):
        key = listOfPossibleKeys[0]
    return key
#==========================================================================================================================
#========================================== UTILITY FUNCTIONS =============================================================
#==========================================================================================================================


TableFileName.write("      GROUP          :       LABEL     :    OUTPUT     \n")

#Group Implemenation description
#    Each group contains the following variables
#        A list named as <GroupName> i.e personGroup, DateGroup etc.
#        A dictionary/map named as <GroupName>Dictionary or <GroupName>Dict.
#    When we iterate through the sentences i.e listOfSentences, we check if the sentence falls into the current group based
#    on conditions specific to the group. A sentence may fall into 2 or more groups if it satisfies the condtion for segregation for
#    the groups. If the sentence falls in to the current group, we find the label which the sentence belongs to ( again the
#    criterion for finding the lable depends on the group ).
#
#    Consider that sentence 's' matches current group and the label for the sentence is 'l'
#    In the <GroupName>Dictionary/Dict we check if the key l is present. If it is not present, we create a new key/value pair
#    for in the <GroupName>Dictionary/Dict where key is 'l' and value is a list. if the key l is already present, that means
#    we have already created the key/value pair in one of the previous iterations. Thus we only need to append the the list.
#    The sample snippet of code for the above is as follows (taken from location group)
#
#            if( key not in locationTableDict):
#                locationTableDict[key] = list()
#            locationTableDict[key].append(sentence + ".")
#
#    Note : We add a fullstop to the sentence every time we append the sentence to a list because we had removed the full stop
#           when we were splitting the contents of the file. See line 34 and its corresponding documentation.
#
#    After each group gets its share of sentences, we output the sentences on console and 2 other output files in a particular
#    format. Format depends on the type of file. eg: TableTCL.txt would have a table formatted output.
#    Exception Date Group -> It does not need to have dictionary as there are only 2 defined labels AM , PM

#Sentiment Group
print("=========================== Sentiment group ========================================\n\n")
outputFile.write("=========================== Sentiment group ========================================\n\n")
TableFileName.write("\nSENTIMENTS    \n                     ")
#TableFileName.write(" SENTIMENTS       ")
sentimentGroup = []
sentimentGroupDictionary = {}
JoyWords = [ "happy", "joy" ]
SadWords = [ "unhappy","sad" ]
AngerWords = [ "Angry" ]
TrustWords = ["trust", "Agree" ]
FearWords = ["Afraid","Scared","panic"]
SurpriseWords = ["Surprise","Marvel","Wonder","Stunner","Shock"]
DisgustWords = ["Dislike","Aversion","revulsion","horror","Dissatisfaction"]
AnticipationWords = ["Expectation","outlook","Prospect","Expect","Expectancy"]
JoyWords = [ word.lower() for word in JoyWords]
SadWords = [ word.lower() for word in SadWords]
AngerWords = [ word.lower() for word in AngerWords]
TrustWords = [ word.lower() for word in TrustWords]
FearWords = [ word.lower() for word in FearWords]
SurpriseWords = [ word.lower() for word in SurpriseWords]
DisgustWords = [ word.lower() for word in DisgustWords]
AnticipationWords = [ word.lower() for word in AnticipationWords]

sentimentGroupDictionary["Joy_Words"] = list()
sentimentGroupDictionary["Sad_Words"] = list()
sentimentGroupDictionary["Anger_Words"] = list()
sentimentGroupDictionary["Trust_Words"] = list()
sentimentGroupDictionary["Fear_Words"] = list()
sentimentGroupDictionary["Surprise_Words"] = list()
sentimentGroupDictionary["Disgust_Words"] = list()
sentimentGroupDictionary["Anticipation_Words"] = list()

for sentence in listOfSentences:
    tempSentenceWords = str(sentence.lower()).split(' ')
    if( set(tempSentenceWords) & set(JoyWords) ):
        sentimentGroupDictionary["Joy_Words"].append(sentence + ".")
        print(len(sentimentGroupDictionary["Joy_Words"]))
    if( set(tempSentenceWords) & set(SadWords) ):
	    sentimentGroupDictionary["Sad_Words"].append(sentence + ".")
    if( set(tempSentenceWords) & set (AngerWords) ):
	    sentimentGroupDictionary["Anger_Words"].append(sentence +".")
    if( set(tempSentenceWords) & set (TrustWords) ):
	    sentimentGroupDictionary["Trust_Words"].append(sentence +".")
    if( set(tempSentenceWords) & set (FearWords) ):
	    sentimentGroupDictionary["Fear_Words"].append(sentence +".")
    if( set(tempSentenceWords) & set (SurpriseWords) ):
	    sentimentGroupDictionary["Surprise_Words"].append(sentence +".")
    if( set(tempSentenceWords) & set (DisgustWords) ):
	    sentimentGroupDictionary["Disgust_Words"].append(sentence +".")
    if( set(tempSentenceWords) & set (AnticipationWords) ):
	    sentimentGroupDictionary["Anticipation_Words"].append(sentence +".")
		
for key in sentimentGroupDictionary:
    x = sentimentGroupDictionary[key];
    print (key + " : ")
	#space manage infront of key
    TableFileName.write(": " + key + "        :    ")
    outputFile.write(key + "    : " + "\n")
    for sent in x:
        sentimentGroup.append(sent + ".")
        print ("    " + sent);
        outputFile.write( "    " + sent + "\n")
        TableFileName.write(sent +  "\n                     :                  : ");
    TableFileName.write("\n                     ")


#Person Group
print("=========================== Person group ========================================\n\n")
outputFile.write("=========================== Person group ========================================\n\n")
TableFileName.write("\n\nPERSON ( Names )    \n                     ")

#The following stop words are not nouns. If after filtering out the nouns using nltk, we get these words,
# we remove them.
stopWords = ['HEY\ud83d\ude02' , 'Hey\ud83d\ude02' ,'Lol\ud83d\ude02','Today','today','Tomarrow','tomarrow','Hi' , 'hi' , 'am','Working in', 'Located' , 'Going to' , 'Stay in','January', 'Jan' , 'February' , 'Feb' , 'March' , 'Mar' , 'April' , 'Apr', 'May', 'May' , 'June' , 'Jun' , 'July' , 'Jul' , 'August' , 'Aug' , 'September' , 'Sep' , 'October' , 'Oct' , 'November' , 'Nov' , 'December' , 'Dec'] #add new words here
personGroup = []
personGroupDictionary = {}
for sentence in listOfSentences:
    tempSentence = str(sentence) # copy the whole sentence in tempSentences to keep the original sentence intact
    tempSentenceWords = tempSentence.split(' ')
    tempSentenceWords = [word for word in tempSentenceWords if word not in stopWords ]
    #We use nltk module to find out all the nouns in the sentence
    tokens = nltk.word_tokenize(sentence)
    tagged = nltk.pos_tag(tokens)
    nouns = [word for word,pos in tagged \
        if (pos == 'NN' or pos == 'NNP' or pos == 'NNS' or pos == 'NNPS')]
    correctNouns = list(set(nouns) - set(stopWords))
    if len(correctNouns) > 0 and len(tempSentenceWords) > 0 and len(tempSentenceWords[0]) > 1:
        # Criterion for label -> The first noun in the sentence.
        nounsWithCapLetters = [ str(noun.upper()[0] + noun.lower()[1::]) for noun in correctNouns]
        firstConsideredWord = str(tempSentenceWords[0].upper()[0] + tempSentenceWords[0].lower()[1::])
        firstConsideredWord = firstConsideredWord.replace(',','')
        firstWordIsNoun = set([firstConsideredWord]) & set(nounsWithCapLetters)
        if(firstWordIsNoun):
            noun = firstConsideredWord
            if noun not in personGroupDictionary:
                personGroupDictionary[noun] = list();
            personGroupDictionary[noun].append( sentence + "." )
            personGroup.append(sentence + '.')

for key in sorted(personGroupDictionary):
    x = personGroupDictionary[key];
    print (key + " : ")
    TableFileName.write(": " + key + "            :    ")
    outputFile.write(key + "    : " + "\n")
    for sent in x:
        print ("    " + sent);
        outputFile.write( "    " + sent + "\n")
        TableFileName.write(sent +  "\n                     :                 : ");
    TableFileName.write("\n                     ")

#Location grouping Desciption
#    There needs to be a special handling for locationPhrases because the phrase is not a list of single words
#    In order to match the phrase, we need to do the following.
#    if the sentence is a list of words ['w1' , 'w2' , 'w3' , 'w4'], we need to create a list that is something like the following
#    ['w1' , 'w2' , 'w3' , 'w4' , 'w1 w2' , 'w2 w3' , 'w3 w4']. If the phrase list contains words that are 1,2,3 letter words
#    then we need to form a list like ['w1' , 'w2' , 'w3' , 'w4' , 'w1 w2' , 'w2 w3' , 'w3 w4' , 'w1 w2 w3' 'w2 w3 w4'].
print("=========================== Location group ========================================\n\n")
outputFile.write("=========================== Location group ========================================\n\n")
locationGroup = []
locationTableDict = {}
locationPhrases = ['Working in', 'Located' , 'Going to' , 'Stay in']
locationPhrasesLower = [x.lower() for x in locationPhrases]

for sentence in listOfSentences:
    tempSentenceLower = str(sentence.lower()) # copy the whole sentence in tempSentences to keep the original sentence intact
    tempSentenceWordsLower = tempSentenceLower.split(' ')
    tempSentenceWordsLower = [x.replace(',','') for x in tempSentenceWordsLower]
    
    CombinationWordList = []

    # tempSentenceWordsLower --> a,b,c,d
    for i in list(range(len(tempSentenceWordsLower) - 1)):
        CombinationWordList.append( tempSentenceWordsLower[i] + ' ' + tempSentenceWordsLower[i+1] )
    # CombinationWordList --> a b, b c , c d

    singleCombinationLower = tempSentenceWordsLower
    tempSentenceWordsLower = singleCombinationLower + CombinationWordList
    if( set(tempSentenceWordsLower) & set(locationPhrasesLower)):
        LocationNameIndex = -1
        matchingSetDoubleWords = set(CombinationWordList) & set(locationPhrasesLower)
        if( len(list(matchingSetDoubleWords)) > 0 ):
            matchingSet = matchingSetDoubleWords
            LocationNameIndex = CombinationWordList.index(list(matchingSet)[0]) + 1
        else:
            matchingSet = set(singleCombinationLower) & set(locationPhrasesLower)
            LocationNameIndex = singleCombinationLower.index(list(matchingSet)[0]) + 1
        key = ""
        if( len(singleCombinationLower) <= LocationNameIndex + 1):
            key = "Unknown"
        else:
            key = singleCombinationLower[LocationNameIndex + 1] # check for capital letters here
        locationGroup.append(sentence + '.')
        if( key not in locationTableDict):
            locationTableDict[key] = list()
        locationTableDict[key].append(sentence + ".")

TableFileName.write("\n\nLOCATION ( CITIES )  ")
for key in sorted(locationTableDict):
    #In order to make the name have first letter capital, we covert the word in to a list of chars, convert the first char
    # to upper case and append the reset of the chars in the list to the capital char. Then we convert it back to a string
    # The concept used in the code snippet containing ---> [1::] <--- is called slicing of a list.
    outputKey = str(list(key)[0].upper()) + ''.join(list(key)[1::])
    print(outputKey)
    TableFileName.write(": " + outputKey + "        :    ")
    for sent in locationTableDict[key]:
        TableFileName.write(sent +  "\n                     :                    : ");
    TableFileName.write("\n                     ")

for x in locationGroup:
    print(x)        
    outputFile.write(x + "\n")

#Date grouping
print("=================Date======================================\n\n")
outputFile.write("=================Date======================================\n\n")
monthWords = ['January', 'Jan' , 'February' , 'Feb' , 'March' , 'Mar' , 'April' , 'Apr', 'May', 'May' , 'June' , 'Jun' , 'July' , 'Jul' , 'August' , 'Aug' , 'September' , 'Sep' , 'October' , 'Oct' , 'November' , 'Nov' , 'December' , 'Dec']
dateGroup = []
DateTableDict = {}
for sentence in listOfSentences:
    tempSentence = str(sentence) # copy the whole sentence in tempSentences to keep the original sentence intact
    tempSentenceWords = tempSentence.split(' ')
    intersectionSet = set(tempSentenceWords) & set(monthWords)
    if( intersectionSet ):
         dateGroup.append(sentence + '.')
         fillDictionary(intersectionSet, DateTableDict, monthWords, sentence + '.')

TableFileName.write("\n\n DATE ( MONTH )      ")
repeatTime = 0

#We iterate through the sorted list of dictionary. The criterion for sort is the month.
# the lamda function would return a lesser value for Jan than feb. Thus making the dictionary sorted based on the
# month type.
for key in sorted(DateTableDict , key=lambda month: monthWords.index(month)):
    if(repeatTime == 0):
        repeatTime = 1
    else:
        TableFileName.write("                     ")
    TableFileName.write(":     " + key + "         : ")
    for sent in DateTableDict[key]:
        TableFileName.write(sent +  "\n                     :                 : ");
    TableFileName.write("\n")

for y in dateGroup:
    print(y)      
    outputFile.write(y + "\n")
#Time grouping
print("=================Time======================\n\n")
outputFile.write("=================Time======================\n\n")
TableFileName.write("\n\n TIME ( AM / PM )    ")
timeWords =['am','pm']
timeWordsLower = [x.lower() for x in timeWords]

timeGroup = []
amList = []
pmList = []
for sentence in listOfSentences:
    tempSentence = str(sentence.lower()) # copy the whole sentence in tempSentences to keep the original sentence intact
    tempSentenceWords = tempSentence.split(' ')
    intersectionList = list(set(tempSentenceWords) & set(timeWordsLower))
    sentenceAppendable = False;
    for intersectionElement in intersectionList:
       indices = [i for i,x in enumerate(tempSentenceWords) if x == intersectionElement ]
       for index in indices:
           if index > 0 and re.search("[0-9][0-9]?(:[0-9][0-9])?",tempSentenceWords[index - 1]):
                sentenceAppendable = True
    if sentenceAppendable:
       timeGroup.append(sentence + '.')
       if list(set(tempSentenceWords) & set(['am'])) :
          amList.append(sentence + '.')
       elif list(set(tempSentenceWords) & set(['pm'])) :
          pmList.append(sentence + ".")

TableFileName.write(":     AM          : ")
for sent in amList:
    TableFileName.write(sent +  "\n                     :                 : ");
TableFileName.write("\n")

TableFileName.write("                     :     PM          : ")
for sent in pmList:
    TableFileName.write(sent +  "\n                     :                 : ");
               
               
for z in timeGroup:
    print(z)
    outputFile.write(z + "\n")

#Not found group
print("==============not match any group===============\n\n")
outputFile.write("==============not match any group===============\n\n")

listOFSentencesWithFullStop = [x + '.' for x in listOfSentences]
DefaultGroup = list((set(listOFSentencesWithFullStop) - (set(personGroup) | set(locationGroup) |  set(dateGroup) | set(timeGroup) | set(sentimentGroup) )))
#create labels for the default group
DefaultGroupDict = {}

DefaultGroupstopWords = ['Am', 'am' , 'Hi' , 'hi' , 'From' , 'from' , 'I' , 'i', 'Hi.', 'hi.'] #add new words here

for sentence in DefaultGroup:
    key = getDefaultGroupMatchedKey(sentence, DefaultGroupstopWords)
    if key not in DefaultGroupDict:
        DefaultGroupDict[key] = list()
    DefaultGroupDict[key].append(sentence + ".")

TableFileName.write("\n\n DEFAULT  GROUP      ")

repeatTimeDefault = 0
for key in sorted(DefaultGroupDict):
    if(repeatTimeDefault == 0):
        repeatTimeDefault = 1
    else:
        TableFileName.write("                     ")
    TableFileName.write(":     " + key + "         : ")
    for sent in DefaultGroupDict[key]:
        TableFileName.write(sent +  "\n                     :                 : ");
    TableFileName.write("\n")

for z in DefaultGroup:
    print(z)
    outputFile.write(z + "\n")

outputFile.close()
TableFileName.close()

print(len(sentimentGroup))


#py.sign_in('sameer3232', 'q0bcrh4Na5GK4ffn3HmL')
py.sign_in('zameer3232', 'eJEVXvqV0J7nQP5RDasb')

#plotly.tools.set_credentials_file(username='sameer3232', api_key='q0bcrh4Na5GK4ffn3HmL')
#plotly.tools.set_config_file(world_readable=True,sharing='public')
# zameer3232 eJEVXvqV0J7nQP5RDasb

fig1 = {
    'data': [{'labels': ['Sentiment','Person', 'Location', 'Date' , 'Time', 'Default'],
             'values': [len(sentimentGroup), len(personGroup), len(locationGroup), len(dateGroup), len(timeGroup)],
             'type': 'pie'}],
        'layout': {'title': 'Text Mining'},
	
        }
fig2 = {
     'data': [{'labels': ['Joy_Words','Sad_Words','Anger_Words','Trust_Words','Fear_Words','Surprise_Words','Disgust_Words','Anticipation_Words'],
             'values': [len(sentimentGroupDictionary["Joy_Words"]),len(sentimentGroupDictionary["Sad_Words"]),len(sentimentGroupDictionary["Anger_Words"]),len(sentimentGroupDictionary["Anticipation_Words"]),len(sentimentGroupDictionary["Trust_Words"]),len(sentimentGroupDictionary["Fear_Words"]),len(sentimentGroupDictionary["Surprise_Words"]),len(sentimentGroupDictionary["Disgust_Words"])],
             'type': 'pie'}],
        'layout': {'title': 'sentiment Mining'},
		}
py.plot(fig1, filename = 'file_test1')
py.plot(fig2, filename = 'file_test2')





