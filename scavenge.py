#Search for string message in the entire data obtained using crawling.
#Note: This code is written keeping in mind that "message:" is present in every string that we want to analysis. Also the string that we analyse
#	   ends with a \n

f = open("cleandata.txt","r");
filedata = f.read()
f.close()

messageToken = "message: "

messageLines = [line + "." for line in filedata.split('\n') if messageToken in line]

#avoid the message word 
messageLines = [line[line.index(messageToken) + len(messageToken) : ] for line in messageLines]
#avoid duplicate

finalMessages = set(messageLines)

for message in finalMessages:
    print( message ) 
	
f = open("input_file.txt","a");

for message in finalMessages:
    f.write(message + "\n") 
	
	
f.close()