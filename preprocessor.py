#2. pre-processing: find and replace unstructure words ex: @=in ,g98=good night

import re

f = open("input_file.txt","r");
filedata = f.read()
f.close()

#Syntactic decomposition
listOfSentences = []

listOfStrings = filedata.split('.')
for string in listOfStrings:
    if string != '':
        listOfSentences.append(string)
        listOfSentences=list(set(listOfSentences))

syntacticDecompositionOutput = []
for sentence in listOfSentences:
    sentence = sentence.lstrip()
    sentence = sentence.rstrip()
    sentence = re.sub('\n','',sentence)
    syntacticDecompositionOutput.append(sentence)

#preprocessing    
#Add your mapping here , ['oldstring', 'newString']
preprocessorMap = [['@','in'],['br','brother'], ['nt','good night'],['gr8','great']]

preProcessingOutput = []
for sentence in syntacticDecompositionOutput:
    for x in preprocessorMap:
        sentence = re.sub(x[0],x[1],sentence)
    preProcessingOutput.append(sentence + '.' + '\n\n')

outputFile = open("input_file_preprocessed.txt","w")
for sentence in preProcessingOutput:
    outputFile.write(sentence)
outputFile.close()
