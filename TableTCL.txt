      GROUP          :       LABEL     :    OUTPUT     

SENTIMENTS    
                     : Joy_Words        :    sameer is happy.
                     :                  : rahim is happy.
                     :                  : 
                     : Sad_Words        :    rahim is sad.
                     :                  : sad man.
                     :                  : 
                     : Anger_Words        :    Angry man.
                     :                  : sameer is very angry.
                     :                  : 
                     : Trust_Words        :    sameer is agree your words.
                     :                  : sameer is trust you.
                     :                  : 
                     : Fear_Words        :    Rahul Afraid by seeing lion.
                     :                  : sameer scared by seeing your shadow Scared.
                     :                  : 
                     : Surprise_Words        :    jogg falls is Wonder full.
                     :                  : i Surprise by seeing your marks.
                     :                  : 
                     : Disgust_Words        :    sameer is Dislike your taste.
                     :                  : 
                     : Anticipation_Words        :    my Expectation is getting good marks.
                     :                  : 
                     

PERSON ( Names )    
                     : Hey\ud83d\ude02            :    HEY\ud83d\ude02.
                     :                 : 
                     : Jogg            :    jogg falls is Wonder full.
                     :                 : 
                     : Lol\ud83d\ude02            :    Lol\ud83d\ude02.
                     :                 : 
                     : Rahim            :    rahim is sad.
                     :                 : rahim is happy.
                     :                 : 
                     : Rahul            :    Rahul Afraid by seeing lion.
                     :                 : 
                     : Raju            :    raju eat food.
                     :                 : raju working in Bangalore.
                     :                 : raju eat banana.
                     :                 : raju drink pepsi.
                     :                 : Raju stay in Tumakuru.
                     :                 : raju stay in mysore.
                     :                 : 
                     : Ramu            :    Ramu eating banana.
                     :                 : Ramu is having nano car.
                     :                 : Ramu not having mobile now.
                     :                 : ramu stay in mysore.
                     :                 : hi Ramu wegood night his home.
                     :                 : 
                     : Sameer            :    sameer is having nano car.
                     :                 : sameer great.
                     :                 : sameer going to mysore.
                     :                 : sameer is happy.
                     :                 : sameer is agree your words.
                     :                 : sameer not having mobile now.
                     :                 : sameer is trust you.
                     :                 : sameer is bad.
                     :                 : sameer scared by seeing your shadow Scared.
                     :                 : sameer brother zam.
                     :                 : sameer is Dislike your taste.
                     :                 : sameer eating banana.
                     :                 : sameer is very angry.
                     :                 : Sameer in college.
                     :                 : 
                     : Things            :    Things always get crazy with these ppl around! Hav walked Several miles fighting.
                     :                 : 
                     : Zameer            :    zameer  tell canvas mobile give heat issue.
                     :                 : zameer is going to magalore.
                     :                 : zameer buy canvas mobile.
                     :                 : 
                     

LOCATION ( CITIES )  : Bangalore        :    raju working in Bangalore.
                     :                    : 
                     : Magalore        :    zameer is going to magalore.
                     :                    : 
                     : Mysore        :    sameer going to mysore.
                     :                    : ramu stay in mysore.
                     :                    : raju stay in mysore.
                     :                    : 
                     : Tumakuru        :    Raju stay in Tumakuru.
                     :                    : 
                     

 DATE ( MONTH )      :     Jan         : Jan 26 republic day.
                     :                 : Jan 23 bharath birthday.
                     :                 : Jan 31 days.
                     :                 : Jan 15 holiday.
                     :                 : 
                     :     Feb         : in Feb 29 days.
                     :                 : in Feb sem exam.
                     :                 : Feb 9 janu bithday.
                     :                 : 
                     :     Jun         : in Jun 1 school open.
                     :                 : June fully rainy season.
                     :                 : 
                     :     Dec         : in  December 31 days.
                     :                 : In December fully cold.
                     :                 : 


 TIME ( AM / PM )    :     AM          : tomarrow 10 am aca class.
                     :                 : tomarrow 9 am DM class.
                     :                 : 
                     :     PM          : yestarday 3 pm cc class.
                     :                 : today 2 pm cc class.
                     :                 : 

 DEFAULT  GROUP      :     *generic         : *Generic friendship quote*..
                     :                 : 
                     :     angry         : Angry man..
                     :                 : 
                     :     doesn't         : Doesn't matter..
                     :                 : 
                     :     dope!.         : Dope!..
                     :                 : 
                     :     expectation         : my Expectation is getting good marks..
                     :                 : 
                     :     for         : For gods sake! They were on a brothereak!..
                     :                 : 
                     :     good         : Good morning People!..
                     :                 : Good Morning Sir..
                     :                 : Good Job Gal!..
                     :                 : good night good good night..
                     :                 : 
                     :     hate         : we hate you..
                     :                 : I hate them! \ud83d\ude13..
                     :                 : I hate man..
                     :                 : 
                     :     hate.         : I hate..
                     :                 : 
                     :     love         : I love Dogs!..
                     :                 : 
                     :     may         : may 9 sameer birth dayjan 4 ram birth day..
                     :                 : 
                     :     progressing         : I am progressing slowly and steadily..
                     :                 : 
                     :     rak         : rak good night ra..
                     :                 : rak good evening..
                     :                 : 
                     :     sad         : sad man..
                     :                 : 
                     :     surprise         : i Surprise by seeing your marks..
                     :                 : 
                     :     that         : That feeling!..
                     :                 : 
                     :     true         : True foreva!..
                     :                 : 
                     :     unKnown         : hi hi..
                     :                 : 
                     :     very         : am very bad..
                     :                 : 
                     :     when         : When the sun goes down..
                     :                 : When you get to stand beside the cutest of all! Nitin Raj..
                     :                 : 
